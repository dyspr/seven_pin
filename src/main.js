var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var numOfRect = 121
var initSize = 0.1
var numOfRow = 7
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var j = 0; j < numOfRow; j++) {
    push()
    translate((j - Math.floor(numOfRow * 0.5)) * boardSize * initSize, 0)
    for (var i = 0; i < numOfRect; i++) {
      fill(128 + 128 * sin((i / numOfRect) + frame * 16 + (j % 2) * Math.PI * 0.5))
      noStroke()
      push()
      translate(windowWidth * 0.5, windowHeight * 0.5 + (i - Math.floor(numOfRect * 0.5)) * initSize * boardSize * 0.05 + boardSize * initSize * sin(frame + (j % 2) * Math.PI * 0.5))
      ellipse(0, 0, boardSize * initSize * (0.5 + 0.5 * sin(frame + i * 0.075 + Math.PI * (j % 2))), boardSize * initSize * (0.5 + 0.5 * sin(frame + i * 0.075 + Math.PI * (j % 2))))
      pop()
    }
    pop()
  }

  frame += deltaTime * 0.001
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
